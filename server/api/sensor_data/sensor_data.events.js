/**
 * SensorData model events
 */

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _events = require('events');

var _sensor_data = require('./sensor_data.model');

var _sensor_data2 = _interopRequireDefault(_sensor_data);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var SensorDataEvents = new _events.EventEmitter();

// Set max event listeners (0 == unlimited)
SensorDataEvents.setMaxListeners(0);

// Model events
var events = {
  'save': 'save',
  'remove': 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  _sensor_data2.default.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function (doc) {
    SensorDataEvents.emit(event + ':' + doc._id, doc);
    SensorDataEvents.emit(event, doc);
  };
}

exports.default = SensorDataEvents;