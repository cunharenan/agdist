'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _sensor = require('../sensor/sensor.model');

var _sensor2 = _interopRequireDefault(_sensor);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var SensorDataSchema = new _mongoose2.default.Schema({
  date: Date,
  value: Number,
  sensor: {
    type: _mongoose2.default.Schema.Types.ObjectId,
    ref: 'Sensor'
  }
});

exports.default = _mongoose2.default.model('SensorData', SensorDataSchema);