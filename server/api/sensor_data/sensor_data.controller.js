/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/sensors_data              ->  index
 * POST    /api/sensors_data              ->  create
 * GET     /api/sensors_data/:id          ->  show
 * PUT     /api/sensors_data/:id          ->  update
 * DELETE  /api/sensors_data/:id          ->  destroy
 */

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.index = index;
exports.show = show;
exports.lastBySensor = lastBySensor;
exports.view = view;
exports.create = create;
exports.insertMultipleBySensor = insertMultipleBySensor;
exports.insertBySensor = insertBySensor;
exports.update = update;
exports.destroy = destroy;

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

var _sensor_data = require('./sensor_data.model');

var _sensor_data2 = _interopRequireDefault(_sensor_data);

var _sensor = require('../sensor/sensor.model');

var _sensor2 = _interopRequireDefault(_sensor);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function (entity) {
    if (entity) {
      return res.status(statusCode).json(entity);
    }
  };
}

function saveUpdates(updates) {
  return function (entity) {
    var updated = _lodash2.default.merge(entity, updates);
    return updated.save().then(function (updated) {
      return updated;
    });
  };
}

function removeEntity(res) {
  return function (entity) {
    if (entity) {
      return entity.remove().then(function () {
        res.status(204).end();
      });
    }
  };
}

function handleEntityNotFound(res) {
  return function (entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function (err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of Sensors
function index(req, res) {
  return _sensor_data2.default.find().exec().then(respondWithResult(res)).catch(handleError(res));
}

// Gets a single SensorData from the DB
function show(req, res) {
  return _sensor_data2.default.findById(req.params.id).exec().then(handleEntityNotFound(res)).then(respondWithResult(res)).catch(handleError(res));
}

// Gets the last by sensor
function lastBySensor(req, res) {
  return _sensor_data2.default.find({
    'sensor': req.params.id
  }).sort('-date').limit(1).exec().then(respondWithResult(res)).catch(handleError(res));
}

// Gets a list of sensor data.
function view(req, res) {
  return _sensor_data2.default.find({
    'sensor': req.query.id,
    'date': { '$gte': req.query.date_start, '$lte': req.query.date_end }
  }).sort('-date').exec().then(respondWithResult(res)).catch(handleError(res));
}

// Creates a new SensorData in the DB
function create(req, res) {
  return _sensor_data2.default.create(req.body).then(respondWithResult(res, 201)).catch(handleError(res));
}

// Creates a new SensorData in the DB based on sensor ID
function insertMultipleBySensor(req, res) {
  var data = req.body;
  _lodash2.default.forEach(data, function (s) {
    _sensor2.default.findOne({ alias: s.alias }).exec().then(function (sensor) {
      if (sensor) {
        s.date = (0, _moment2.default)(s.date, 'DD/MM/YYYY HH:mm:ss').toDate();
        var sensorData = new _sensor_data2.default();
        sensorData.date = s.date;
        sensorData.value = s.value;
        sensorData.sensor = sensor._id;
        sensorData.save();
        var socketio = req.app.get('socketio');
        socketio.sockets.emit('data_arrived:' + sensor._id, sensorData);
      }
    });
  });

  return res.status(200).end();
}

// Creates a new SensorData in the DB based on sensor ID
function insertBySensor(req, res) {
  var dateStr = req.body.date,
      date = (0, _moment2.default)(dateStr, 'DD/MM/YYYY HH:mm:ss').toDate();

  // Convert date
  req.body.date = date;

  return _sensor2.default.findOne({ alias: req.body.alias }).exec().then(function (sensor) {
    if (sensor) {
      var sensorData = new _sensor_data2.default();
      sensorData.date = req.body.date;
      sensorData.value = req.body.value;
      sensorData.sensor = sensor._id;
      return sensorData.save();
    } else {
      res.status(404).end();
      return null;
    }
    return sensor;
  }).then(respondWithResult(res)).catch(handleError(res));
}

// Updates an existing SensorData in the DB
function update(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  return _sensor_data2.default.findById(req.params.id).exec().then(handleEntityNotFound(res)).then(saveUpdates(req.body)).then(respondWithResult(res)).catch(handleError(res));
}

// Deletes a SensorData from the DB
function destroy(req, res) {
  return _sensor_data2.default.findById(req.params.id).exec().then(handleEntityNotFound(res)).then(removeEntity(res)).catch(handleError(res));
}