'use strict';

// Production specific configuration
// =================================

module.exports = {
        // Server IP
        ip: process.env.OPENSHIFT_NODEJS_IP || process.env.IP || undefined,

        // Server port
        port: process.env.OPENSHIFT_NODEJS_PORT || process.env.PORT || 8080,

        // MongoDB connection options
        mongo: {
                uri: 'mongodb://aguser:123456789@ds011734.mlab.com:11734/agweb'
        },

        seedDB: false
};