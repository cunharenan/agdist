/**
 * Populate DB with sample data on server start
 * to disable, edit config/environment/index.js, and set `seedDB: false`
 */

'use strict';

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

var _thing = require('../api/thing/thing.model');

var _thing2 = _interopRequireDefault(_thing);

var _sensor = require('../api/sensor/sensor.model');

var _sensor2 = _interopRequireDefault(_sensor);

var _sensor_data = require('../api/sensor_data/sensor_data.model');

var _sensor_data2 = _interopRequireDefault(_sensor_data);

var _user = require('../api/user/user.model');

var _user2 = _interopRequireDefault(_user);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_sensor2.default.find({}).remove().then(function () {
  _sensor2.default.create({
    name: 'Umidade 1',
    description: 'Descrição do sensor',
    alias: 'umidade1'
  }, {
    name: 'Temperatura 1',
    description: 'Descrição do sensor',
    alias: 'temperatura1'
  }, {
    name: 'Umidade 2',
    description: 'Descrição do sensor',
    alias: 'umidade2'
  }, {
    name: 'Temperatura 2',
    description: 'Descrição do sensor',
    alias: 'temperatura2'
  }, {
    name: 'Chuva',
    description: 'Descrição do sensor',
    alias: 'chuva'
  }, {
    name: 'LDR',
    description: 'Descrição do sensor',
    alias: 'ldr'
  }).then(function () {
    _sensor2.default.findOne().exec().then(function (sensor) {

      _sensor_data2.default.find({}).remove().then(function () {

        var date = (0, _moment2.default)();

        for (var i = 0; i < 0; i++) {
          var _date = date.add(10, 'minutes');
          var sensorData = new _sensor_data2.default();
          sensorData.date = _date;
          sensorData.value = Math.random() * 30;
          sensorData.sensor = sensor._id;

          sensorData.save();
        }
      });
    });
  });
});

_thing2.default.find({}).remove().then(function () {
  _thing2.default.create({
    name: 'Development Tools',
    info: 'Integration with popular tools such as Bower, Grunt, Babel, Karma, ' + 'Mocha, JSHint, Node Inspector, Livereload, Protractor, Jade, ' + 'Stylus, Sass, and Less.'
  }, {
    name: 'Server and Client integration',
    info: 'Built with a powerful and fun stack: MongoDB, Express, ' + 'AngularJS, and Node.'
  }, {
    name: 'Smart Build System',
    info: 'Build system ignores `spec` files, allowing you to keep ' + 'tests alongside code. Automatic injection of scripts and ' + 'styles into your index.html'
  }, {
    name: 'Modular Structure',
    info: 'Best practice client and server structures allow for more ' + 'code reusability and maximum scalability'
  }, {
    name: 'Optimized Build',
    info: 'Build process packs up your templates as a single JavaScript ' + 'payload, minifies your scripts/css/images, and rewrites asset ' + 'names for caching.'
  }, {
    name: 'Deployment Ready',
    info: 'Easily deploy your app to Heroku or Openshift with the heroku ' + 'and openshift subgenerators'
  });
});

_user2.default.find({}).remove().then(function () {
  _user2.default.create({
    provider: 'local',
    name: 'Test User',
    email: 'test@example.com',
    password: 'test'
  }, {
    provider: 'local',
    role: 'admin',
    name: 'Administrador',
    email: 'admin@admin.com',
    password: 'admin'
  }, {
    provider: 'local',
    role: 'admin',
    name: 'Admin',
    email: 'admin@example.com',
    password: 'admin'
  }).then(function () {
    console.log('finished populating users');
  });
});